%global _empty_manifest_terminate_build 0
Name:           python-decorator
Version:        5.1.1
Release:        1
Summary:        Decorators for Humans
License:        BSD
URL:            https://github.com/micheles/decorator
Source0:        https://files.pythonhosted.org/packages/66/0c/8d907af351aa16b42caae42f9d6aa37b900c67308052d10fdce809f8d952/decorator-5.1.1.tar.gz
BuildArch:      noarch
%description
The goal of the decorator module is to make it easy to define signature-preserving
function decorators and decorator factories. It also includes an implementation of multiple dispatch and
other niceties (please check the docs).

%package -n python3-decorator
Summary:        Decorators for Humans
Provides:       python-decorator
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
%description -n python3-decorator
The goal of the decorator module is to make it easy to define signature-preserving
function decorators and decorator factories. It also includes an implementation of multiple dispatch and
other niceties (please check the docs).

%package help
Summary:        Decorators for Humans
Provides:       python3-decorator-doc
%description help
The goal of the decorator module is to make it easy to define signature-preserving
function decorators and decorator factories. It also includes an implementation of multiple dispatch and
other niceties (please check the docs).

%prep
%autosetup -n decorator-%{version}

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%check
%{__python3} setup.py test

%files -n python3-decorator -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu May 19 2022 OpenStack_SIG <openstack@openeuler.org> - 5.1.1-1
- Upgrade package python3-decorator to version 5.1.1

* Sat Nov 13 2021 liudabo<liudabo1@huawei.com> - 5.0.9-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 5.0.9

* Fri Oct 23 2020 tianwei<tianwei12@huawei.com> - 4.4.2-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete python2

* Thu Jul 23 2020 dingyue<dingyue5@huawei.com> - 4.4.2-1
- Type:enhancement
- ID:NA
- SUG:restart
- DESC:update source code

* Tue Dec 31 2019 Yeqing Peng<pengyeqing@huawei.com> - 4.3.0-3
- Type:enhancement
- ID:NA
- SUG:restart
- DESC:update source code

* Tue Sep 10 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.3.0-2
- Package init
